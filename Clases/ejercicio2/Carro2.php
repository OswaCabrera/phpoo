<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $circula;

	
	//declaracion del método verificación
	public function verificacion($anioFabricacion){
		//Establecemos los años limites usando la clase DateTime
		$_1990 = new DateTime("00-01-1990");
		$_2010 = new DateTime("00-01-2010");
		//separo los elementos del valor del formulario tipo "month"
		$anioArray = explode("-",$anioFabricacion);
		//Concateno los valores para obtener el formato requerido para usar la clase DateTime
		$anioTemp = "00-".$anioArray[1]."-".$anioArray[0];
		//Creo un nuevo objeto usando DateTime
		$anio = new DateTime($anioTemp);
		//Comparo que la fecha obtenida sea menor a 1990
		if($anio<$_1990){
			return "No";
		//Comparo que la fecha obtenido sea mayor a 2010
		}elseif($anio>$_2010){
			return "Si";	
		//Si resulta falso en ambos casos, estará entre ambas fechas
		}else{
			return "Revision";
		}

	}

	//Creo el getter y setter 

	public function getCircula(){
		return $this->circula;
	}

	public function setCircula($anio){
		$this->circula = $this->verificacion($anio); ;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	//mando a llamar el setter del atributo circula
	$Carro1->setCircula($_POST['anio']);
}




