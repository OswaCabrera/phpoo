# Ejercicio 2
## Cabrera Pérez Oswaldo

## **Resultados:**

### Cuando el año es mayor a 2010:

![prueba](../../pruebas/prueba_ejer2(1).png)
![prueba2](../../pruebas/prueba_ejer2(2).png)
### Cuando el año está entre 1990 y 2010:
![prueba3](../../pruebas/prueba_ejer2(3).png)
![prueba4](../../pruebas/prueba_ejer2(4).png)
### Cuando el año es menor a 1990:
![prueba5](../../pruebas/prueba_ejer2(5).png)
![prueba6](../../pruebas/prueba_ejer2(6).png)

La vista se encuentra en: [VistaEjercicio2](https://gitlab.com/OswaCabrera/phpoo/-/blob/main/Vistas/vistaEjercicio2.php)