<?php  
//declaracion de clase token
	class token2{
		//declaracion de atributos
		private $nombre;
		private $token;

		//declaracion de metodo constructor
		public function __construct($nombre_front){
			$this->nombre=$nombre_front;
            //Mando a llamar a la función que genera el token
			$this->token= $this->generarToken();
		}

		//declaracion del metodo mostrar para armar el mensaje con el nombre y token
		/*public function mostrar(){
			return 'Hola '.$this->nombre.' este es tu token: '.$this->token;
		}*/


        //declaración de la función que general el token 
        public function generarToken(){
            //arreglo con todas las letras del abecedario en mayúsculas
            $arregloLetras = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"); 
            //Se escoje al azar cuatro letras 
            return $arregloLetras[rand(0,25)].$arregloLetras[rand(0,25)].$arregloLetras[rand(0,25)].$arregloLetras[rand(0,25)];
        }
		//declaracion de metodo destructor
		public function __destruct(){
			//destruye token
			//Imprimo el token con un poco de estilo
			echo "<input class='form-control' style='margin: 1em' type='text' value='El token era: $this->token' readonly>";
			//echo $this->token;
		}
	}



if (!empty($_POST)){
	//creacion de objeto de la clase
	$token1= new token2($_POST['nombre']);
}


?>
