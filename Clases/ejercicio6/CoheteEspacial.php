<?php
class CoheteEspacial extends transporte{
		private $tipo;

		//sobreescritura de constructor
		public function __construct($nom,$vel,$com,$type){
			parent::__construct($nom,$vel,$com);
			$this->tipo=$type;
		}

		// sobreescritura de metodo
		public function resumenCohete(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Tipo de Cohete:</td>
						<td>'. $this->tipo.'</td>				
					</tr>';
			return $mensaje;
		}
	}

?>